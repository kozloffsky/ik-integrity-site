(ns ikintegrity.views.blog.index 
  (:require [hiccup.page :as page]
            [ikintegrity.views.layout :as layout]))

(defn index []
  (layout/main "Test Content"))
