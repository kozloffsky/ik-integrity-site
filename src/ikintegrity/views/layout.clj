(ns ikintegrity.views.layout
  (:require [hiccup.page :as page]))

(defn main [content & title]
  (page/html5
   [:html {:ng-app "application"}
    [:head
     [:title title]
     (page/include-css "http://maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css")
     ;; add AngularJS
     (page/include-js "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.13/angular.min.js")
     (page/include-js "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.13/angular-resource.min.js")
     (page/include-js "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.13/angular-route.min.js")
     ;; add Bootstrap
     (page/include-js "/js/ui-bootstrap-tpls-0.11.0.min.js")
     (page/include-js "/js/app/application.js")]
    [:body
     [:div.navbar.navbar-inverse.navbar-fixed-top {:role "navigation"}
      [:div.container
       [:div.navbar-header
        [:button.navbar-toggle {:type "button" :data-toggle "collapse" :data-target ".navbar-collapse"}
         [:span.sr-only "Toggle navigation"]]
        [:a.navbar-brand {:href "#"} "IK Integrity"]]
       [:div.navbar-collapse.collapse
        [:form.navbar-form.navbar-right {:role "form"}
         [:div.form-group 
          [:input.form-control {:type "text" :placeholder "Email"}]]
         [:div.form-group 
          [:input.form-control {:type "password" :placeholder "Password"}]]
         [:button.btn.btn-success {:type "submit"} "Sign In"]]]]]
     "<div ng-view />"
     [:div#content content]]]))

