(ns ikintegrity.routes
  (require [compojure.core :refer :all]
            [compojure.route :as route]
            [ikintegrity.controllers.blog :as blog]
            [ikintegrity.controllers.api :as api]))


(defroutes app-routes
  (GET "/" [] (blog/index))
  (GET "/blog" [] (blog/index))
;; API routes
  (GET "/api/news" [] (api/news))
  (route/resources "/")
  (route/not-found "FIXME: put here 404 controller call"))


