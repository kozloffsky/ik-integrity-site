(ns ikintegrity.controllers.api
  (require [ikintegrity.views.blog.index :as view]
           [clojure.data.json :as json]))

(defn news []
  (json/write-str 
   [
    {
     :title "Test Title"
     :content "Test Content"
     :id 123465
     }
    {
     :title "Test Title"
     :content "Test Content"
     :id 123465
     }
    {
     :title "Test Title"
     :content "Test Content"
     :id 123465
     }]))
