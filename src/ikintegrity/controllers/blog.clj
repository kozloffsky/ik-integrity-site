(ns ikintegrity.controllers.blog
  (require [ikintegrity.views.blog.index :as view]))

(defn index []
  (view/index))
