(ns ikintegrity.handler
  (require [compojure.core :refer :all]
           [compojure.handler :as handler]
           [compojure.route :as route]
           [ikintegrity.routes :as routes]
           [ikintegrity.db :as db]))

(def app
  (handler/site routes/app-routes))
