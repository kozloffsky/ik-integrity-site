

* Skills Tree
** Web 
*** PHP
**** Symfony 2
**** Silex
**** Doctrine
**** Zend Framework
*** Python
**** Flask
*** Groovy
**** Groovy On Grails
*** HTML5
**** AngularJS
**** Bootstrap
*** Node JS

** Enterprise
*** E-Commerce
**** Magento
*** ERP
**** OpenERP
*** CRM
**** ORO CRM
*** Other Enterprise Solutions
**** Java EE based enterprise solutions
***** Spring and Hibernate based custom enterprise applications
***** Enterprise Java Beans application server based custom enterprise services

** Game Development
*** Massive Multyplayer Online Games
**** Backend
***** NodeJS
***** Clojue
***** Java
**** Frontend
***** HTML5 with canvas
***** Flash
***** Unity3D based Desktop/Mobile solutions
*** Casual
**** Flash or HTML5
*** Social Integration
