angular.module("application", ['ngRoute','ui.bootstrap'])

.config(function($routeProvider) {
    $routeProvider
	.when('/', {
	    controller:'IndexController',
	    templateUrl:'/templates/index.html'
	});
})

.controller('IndexController', ["$scope","$http", function($scope, $http) {
    $scope.carouselInterval = 5000;
    var slides = $scope.slides = [];
    $scope.addSlide = function() {
	var newWidth = 600 + slides.length;
	slides.push({
	    image: 'http://placekitten.com/' + newWidth + '/300',
	    text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
		['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
	});
    };
    //for (var i=0; i<4; i++) {
//	$scope.addSlide();
    //}

    $http.get("/api/news").success(function(data){
	for(var obj in data){
	    var o = data[obj];
	    slides.push(o);
	}
    });
}]);

