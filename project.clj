(defproject ikintegrity "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.8"]
                 [hiccup "1.0.4"]
                 [korma "0.3.0"]
                 [com.h2database/h2 "1.3.170"]
                 [org.clojure/data.json "0.2.5"]
                 [com.cemerick/friend "0.2.1"]]
  
  :plugins [[lein-ring "0.8.11"]]
  :ring {:handler ikintegrity.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}})
